package br.com.syonet.programa;

import java.util.Arrays;
import java.util.List;

import br.com.syonet.entidades.Autor;
import br.com.syonet.entidades.Livro;
import br.com.syonet.exceptions.AutorNaoEncontradoException;
import br.com.syonet.exceptions.LivroNaoEncontradoException;
import br.com.syonet.services.AutorService;
import br.com.syonet.services.LivroService;

public class Main {

	public static void main(String[] args) {

		Autor autor1 = new Autor.AutorBuilder().id(null).nome("Tolkien").sobrenome("F").dataNascimento("16/07/1975")
				.cidade("New York").buid();

		Autor autor2 = new Autor.AutorBuilder().id(null).nome("Maurício").sobrenome("de Souza")
				.dataNascimento("16/08/1967").cidade("São Paulo").buid();

		Autor autor3 = new Autor.AutorBuilder().id(null).nome("Clive").sobrenome("Staples Lewis")
				.dataNascimento("16/07/1945").cidade("Reino Unido").buid();

		System.out.println(autor1);
		System.out.println(autor2);
		System.out.println(autor3);

		AutorService autorService = new AutorService();

		try {
			autorService.salvarAutor(autor1);
			autorService.salvarAutor(autor2);
			autorService.salvarAutor(autor3);
		} catch (AutorNaoEncontradoException e) {
			e.printStackTrace();
		}

		Livro livro = new Livro.LivroBuilder().id(null).titulo("O Senhor dos Anéis - Novo")
				.tituloOriginal("The Fellowship of the Ring new").autores(Arrays.asList(autor1, autor3))
				.momeEditora("Editora").numeroPaginas(564).numeroEdicao(2).buid();

		Livro livro2 = new Livro.LivroBuilder().id(null).titulo("O Senhor dos Anéis - Novo")
				.tituloOriginal("The Fellowship of the Ring new").autores(Arrays.asList(autor1, autor3))
				.momeEditora("Editora").numeroPaginas(564).numeroEdicao(2).buid();

		Livro livro3 = new Livro.LivroBuilder().id(null).titulo("Turma da Mônica").tituloOriginal("Turma da Mônica")
				.autores(Arrays.asList(autor2)).momeEditora("Editora Globo").numeroPaginas(42).numeroEdicao(1).buid();
//		
		System.out.println(livro);

		LivroService livroService = new LivroService();

		try {
			livroService.salvarLivro(livro);
			livroService.salvarLivro(livro2);
			livroService.salvarLivro(livro3);
		} catch (LivroNaoEncontradoException e) {
			e.printStackTrace();
		}

		List<Livro> livro4 = livroService.buscarLivrosPorNomeAutor("TOLKIEN");

		livro4.forEach(System.out::println);

		try {
			livro = livroService.buscarPorId(1);
			livroService.deletar(livro);
		} catch (LivroNaoEncontradoException e) {
			e.printStackTrace();
		}
		
		livro3 = new Livro.LivroBuilder().id(3).titulo("Turma da Cebolinha").tituloOriginal("Turma da Mônica")
				.autores(Arrays.asList(autor2)).momeEditora("Editora Globo").numeroPaginas(34).numeroEdicao(3).buid();
		
		try {
			livroService.atualizar(livro3);
		} catch (LivroNaoEncontradoException e) {
			e.printStackTrace();
		}
	}

}

package br.com.syonet.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.syonet.entidades.Autor;
import br.com.syonet.entidades.Livro;
import br.com.syonet.exceptions.AutorNaoEncontradoException;

public class AutorDAO {

	EntityManagerFactory entityManagerFactory = null;
	EntityManager entityManager = null;

	public void salvarAutor(Autor autor) {
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(autor);
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();
		System.out.println("Autor Salvo");
	}

	public Autor buscarPorId(Integer id) throws AutorNaoEncontradoException {
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		Autor autor = entityManager.find(Autor.class, id);
		return autor;

	}

	public void atualizar(Autor autor) throws AutorNaoEncontradoException {

		Autor autorAtualizado = buscarPorId(autor.getId());
		autorAtualizado.setNome(autor.getNome());
		autorAtualizado.setSobrenome(autor.getSobrenome());
		autorAtualizado.setDataNascimento(autor.getDataNascimento());
		autorAtualizado.setCidade(autor.getCidade());
	
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(autor);
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();
		System.out.println("Livro atualizado");

	}

	public List<Livro> buscarLivrosPorNomeAutor(String nomeAutor) {
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		return entityManager.createQuery("\nselect c from syo_livro c where c.nomeAutor = :pNomeAutor order by 1", Livro.class)
				.setParameter("pNomeAutor", nomeAutor)
				.getResultList();

	}

	public void deletarAutor(Autor autor) throws AutorNaoEncontradoException {

		Autor autorAtualizado = buscarPorId(autor.getId());
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager.getTransaction().begin();
		entityManager.remove(autorAtualizado);
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();
		System.out.println("Autor Deletado");

	}

}

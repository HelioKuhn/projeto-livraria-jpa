package br.com.syonet.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.syonet.entidades.Livro;
import br.com.syonet.exceptions.LivroNaoEncontradoException;

public class LivroDAO {

	EntityManagerFactory entityManagerFactory = null;
	EntityManager entityManager = null;

	public void salvarLivro(Livro livro) {
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.persist(livro);
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();
		System.out.println("livro salvo com sucesso");
	}

	public Livro buscarPorId(Integer id) throws LivroNaoEncontradoException {
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		Livro livro = entityManager.find(Livro.class, id);
		return livro;

	}

	public void atualizar(Livro livro) throws LivroNaoEncontradoException {

		Livro livroAtualizado = buscarPorId(livro.getId());
		livroAtualizado.setTitulo(livro.getTitulo());
		livroAtualizado.setTituloOriginal(livro.getTituloOriginal());
		livroAtualizado.setAutores(livro.getAutores());
		livroAtualizado.setNomeEditora(livro.getNomeEditora());
		livroAtualizado.setNumeroPaginas(livro.getNumeroPaginas());
		livroAtualizado.setNumeroEdicao(livro.getNumeroEdicao());

		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.merge(livro);
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();
		System.out.println("Livro atualizado");

	}

	public List<Livro> buscarLivrosPorNomeAutor(String nomeAutor) {
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager = entityManagerFactory.createEntityManager();
		return entityManager.createQuery("select l from Livro l \n"
				+ "join l.autores a where a.nome =  :pNomeAutor order by 1", Livro.class)
				.setParameter("pNomeAutor", nomeAutor)
				.getResultList();

	}

	public void deletar(Livro livro) throws LivroNaoEncontradoException {

		Livro correntistadeleta = buscarPorId(livro.getId());
		entityManagerFactory = Persistence.createEntityManagerFactory("jpa-livraria");
		entityManager.getTransaction().begin();
		entityManager.remove(correntistadeleta);
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();
		System.out.println("Livro deletado");

	}

}

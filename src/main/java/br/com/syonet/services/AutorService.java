package br.com.syonet.services;

import br.com.syonet.dao.AutorDAO;
import br.com.syonet.entidades.Autor;
import br.com.syonet.exceptions.AutorNaoEncontradoException;
import br.com.syonet.utilitarios.UtilitarioEntidades;

public class AutorService {

	AutorDAO autorDAO = null;

	public AutorService() {
		autorDAO = new AutorDAO();
	}

	public void salvarAutor(Autor autor) throws AutorNaoEncontradoException {
		UtilitarioEntidades.uperCaseAll(autor);

		if (autor.getId() == null) {
			autorDAO.salvarAutor(autor);
		} else {
			atualizarAutor(autor);
		}

	}

	public Autor buscarAutorPorId(Integer id) throws AutorNaoEncontradoException {

		if (id == null) {
			throw new IllegalArgumentException("Valor passado é inválido");
		}
		Autor autor = autorDAO.buscarPorId(id);
		return autor;

	}

	public void atualizarAutor(Autor autor) throws AutorNaoEncontradoException {

		if (autor == null) {
			throw new IllegalArgumentException("Valor passado é inválido");
		}
		autorDAO.atualizar(autor);

	}


	public void deletarAutor(Autor autor) throws AutorNaoEncontradoException {

		if (autor == null) {
			throw new IllegalArgumentException("Valor passado é inválido");
		}
		autorDAO.deletarAutor(autor);

	}

}

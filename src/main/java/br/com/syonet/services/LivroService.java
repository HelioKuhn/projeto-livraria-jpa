package br.com.syonet.services;

import java.util.List;
import java.util.Objects;

import br.com.syonet.dao.LivroDAO;
import br.com.syonet.entidades.Livro;
import br.com.syonet.exceptions.LivroNaoEncontradoException;
import br.com.syonet.utilitarios.UtilitarioEntidades;

public class LivroService {

	LivroDAO livrariaDAO = null;

	public LivroService() {
		livrariaDAO = new LivroDAO();
	}

	public void salvarLivro(Livro livro) throws LivroNaoEncontradoException {
		
		if(Objects.isNull(livro)) {
			throw new IllegalArgumentException("Valor passado no parâmentro não pode ser NULL");
		}
		
		UtilitarioEntidades.uperCaseAll(livro);

		if (livro.getId() == null) {
			livrariaDAO.salvarLivro(livro);
		} else {
			atualizar(livro);
		}

	}

	public Livro buscarPorId(Integer id) throws LivroNaoEncontradoException {

		if (id == null) {
			throw new IllegalArgumentException("Valor passado é inválido");
		}
		Livro livro = livrariaDAO.buscarPorId(id);
		return livro;

	}

	public void atualizar(Livro livro) throws LivroNaoEncontradoException {
	
		if (livro == null) {
			throw new IllegalArgumentException("Valor passado é inválido");
		}
		UtilitarioEntidades.uperCaseAll(livro);
		livrariaDAO.atualizar(livro);

	}

	public List<Livro> buscarLivrosPorNomeAutor(String nomeAutor) {
		if (nomeAutor == null) {
			throw new IllegalArgumentException("Nome do autor solicitado é inválido");
		}
		nomeAutor = nomeAutor.toUpperCase();
		return livrariaDAO.buscarLivrosPorNomeAutor(nomeAutor);

	}

	public void deletar(Livro livro) throws LivroNaoEncontradoException {

		if (livro == null) {
			throw new IllegalArgumentException("Valor passado é inválido");
		}
		livrariaDAO.deletar(livro);

	}

}

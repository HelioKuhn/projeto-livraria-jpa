package br.com.syonet.entidades;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "syo_livro")
public class Livro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "nm_titulo", nullable = false)
	private String titulo;

	@Column(name = "nm_titulooriginal")
	private String tituloOriginal;

	@Column(name = "nm_editora")
	private String nomeEditora;

	@Column(name = "no_paginas")
	private Integer numeroPaginas;

	@Column(name = "no_edicao")
	private Integer numeroEdicao;

	@ManyToMany
	@JoinTable(name = "syo_autorlivro", joinColumns = @JoinColumn(name = "livro_id"), inverseJoinColumns = @JoinColumn(name = "autor_id"))
	private List<Autor> autores;
	
	public Livro() {
		
	}
	
	private Livro(LivroBuilder livroBuilder) {
		this.id = livroBuilder.id;
		this.titulo = livroBuilder.titulo;
		this.tituloOriginal = livroBuilder.tituloOriginal;
		this.autores = livroBuilder.autores;
		this.nomeEditora = livroBuilder.nomeEditora;
		this.numeroPaginas = livroBuilder.numeroPaginas;
		this.numeroEdicao = livroBuilder.numeroEdicao;
	}

	@Override
	public String toString() {
		return "Livro [id=" + id + ", titulo=" + titulo + ", tituloOriginal=" + tituloOriginal + ", autores:="
				+ listarAutores() + "nomeEditora=" + nomeEditora + ", numeroPaginas=" + numeroPaginas
				+ ", numeroEdicao=" + numeroEdicao + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setTituloOriginal(String tituloOriginal) {
		this.tituloOriginal = tituloOriginal;
	}

	public void setNomeEditora(String nomeEditora) {
		this.nomeEditora = nomeEditora;
	}

	public void setNumeroPaginas(Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	public void setNumeroEdicao(Integer numeroEdicao) {
		this.numeroEdicao = numeroEdicao;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getTituloOriginal() {
		return tituloOriginal;
	}

	public String getNomeEditora() {
		return nomeEditora;
	}

	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	public Integer getNumeroEdicao() {
		return numeroEdicao;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Autor> getAutor() {
		return autores;
	}

	public void setAutor(List<Autor> autor) {
		this.autores = autor;
	}

	public void addAutor(Autor autor) {
		autores.add(autor);
	}

	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}

	private String listarAutores() {
		String stringAutores = "";
		for (Autor autor : autores) {
			stringAutores += autor + ", ";
		}
		return stringAutores;
	}

	public static class LivroBuilder {

		private Integer id;
		private String titulo;
		private String tituloOriginal;
		private String nomeEditora;
		private Integer numeroPaginas;
		private Integer numeroEdicao;
		private List<Autor> autores;

		public LivroBuilder id(Integer id) {
			this.id = id;
			return this;
		}

		public LivroBuilder titulo(String titulo) {
			this.titulo = titulo;
			return this;
		}

		public LivroBuilder tituloOriginal(String tituloOriginal) {
			this.tituloOriginal = tituloOriginal;
			return this;
		}

		public LivroBuilder autores(List<Autor> autores) {
			this.autores = autores;
			return this;
		}

		public LivroBuilder momeEditora(String nomeEditora) {
			this.nomeEditora = nomeEditora;
			return this;
		}

		public LivroBuilder numeroPaginas(Integer numeroPaginas) {
			this.numeroPaginas = numeroPaginas;
			return this;
		}

		public LivroBuilder numeroEdicao(Integer numeroEdicao) {
			this.numeroEdicao = numeroEdicao;
			return this;
		}

		public Livro buid() {
			return new Livro(this);
		}

	}

}

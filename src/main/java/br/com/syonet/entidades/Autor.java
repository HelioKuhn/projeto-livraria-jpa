package br.com.syonet.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "syo_autor")
public class Autor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String nome;

	@Column
	private String sobrenome;

	@Column
	private String dataNascimento;

	@Column
	private String cidade;

	public Autor() {

	}

	private Autor(AutorBuilder autorBuilder) {
		this.id = autorBuilder.id;
		this.nome = autorBuilder.nome;
		this.sobrenome = autorBuilder.sobrenome;
		this.dataNascimento = autorBuilder.dataNascimento;
		this.cidade = autorBuilder.cidade;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	@Override
	public String toString() {
		return "Autor [id=" + id + ", nome=" + nome + ", sobrenome=" + sobrenome + ", dataNascimento=" + dataNascimento
				+ ", cidade=" + cidade + "]";
	}

	public static class AutorBuilder {

		private Integer id;
		private String nome;
		private String sobrenome;
		private String dataNascimento;
		private String cidade;

		public AutorBuilder id(Integer id) {
			this.id = id;
			return this;
		}

		public AutorBuilder nome(String nome) {
			this.nome = nome;
			return this;
		}

		public AutorBuilder sobrenome(String sobrenome) {
			this.sobrenome = sobrenome;
			return this;
		}

		public AutorBuilder dataNascimento(String dataNascimento) {
			this.dataNascimento = dataNascimento;
			return this;
		}

		public AutorBuilder cidade(String cidade) {
			this.cidade = cidade;
			return this;
		}

		public Autor buid() {
			return new Autor(this);
		}

	}

}
